package ch02;

public class LogAnalyzer {

    public boolean WasLastFileNameValid = false;

    public boolean isValidLogFileName(String fileName) {
        WasLastFileNameValid = false;
        if (fileName.isEmpty()) {
            throw new IllegalArgumentException("file name is null");
        }
        if (!fileName.endsWith(".slf")) {
            return false;
        }
        WasLastFileNameValid = true;
        return true;
    }
}
