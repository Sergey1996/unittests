package ch08.ex1;

public class AnalyzedOutput {
    public int lineCount;
    public String[] mass;


    public String getLine(int ind) {
        return mass[ind];
    }

    public void addLine(String... s) {
        if (mass == null || mass.length == 0) {
            mass = s;
        }
        lineCount = mass.length;
    }

    @Override
    public boolean equals(Object obj) {
        AnalyzedOutput analyzedOutput = (AnalyzedOutput)obj;
        for(int i = 0; i < mass.length; i++) {
            if(!mass[i].equals(analyzedOutput.mass[i])) {
                return false;
            }
        }
        return true;
    }
}
