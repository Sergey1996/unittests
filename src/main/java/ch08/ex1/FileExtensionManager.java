package ch08.ex1;

public class FileExtensionManager implements IExtensionManager {
    @Override
    public boolean isValid(String name) {
        if (name.endsWith(".exe")) {
            return true;
        }
        /*this custom implementation*/
        return false;
    }
}
