package ch08.ex1;

public class LogAnalyzer {

    private IExtensionManager manager;

    public void init() {
        manager = new FileExtensionManager();
    }

    public boolean isValidLogFileName(String fileName) {
        return manager.isValid(fileName);
    }

    public AnalyzedOutput analyze(String str) {
        AnalyzedOutput analyzedOutput = new AnalyzedOutput();
        analyzedOutput.addLine(str.split("\t"));
        return analyzedOutput;
    }
}
