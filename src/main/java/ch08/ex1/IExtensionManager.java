package ch08.ex1;

public interface IExtensionManager {
    boolean isValid(String name);
}
