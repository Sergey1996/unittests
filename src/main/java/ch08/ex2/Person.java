package ch08.ex2;

import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class Person {
    private List<String> phones;

    public Person() {
        phones = new ArrayList<>();
    }

    public void addNumber(String num) {
        phones.add(num);
    }

    public String findPhoneStartingWith(String num) {
        String rez = null;
        for(String str : phones) {
            if(str.startsWith(num)) {
                rez = str;
            }
        }
        return rez;
    }
}
