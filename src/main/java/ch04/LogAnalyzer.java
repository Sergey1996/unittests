package ch04;

import ch3.IExtensionManager;

public class LogAnalyzer {

    private IWebService service;

    public LogAnalyzer(IWebService service) {
        this.service = service;
    }

    public void analyze(String fileName) throws Exception {
        if (fileName.length() < 8) {
            service.LogError("this is short name: " + fileName);
        }
    }
}
