package ch04;

public class LogAnalyzer2 {
    public IWebService webService;
    public IEmailService emailService;

    public IWebService getWebService() {
        return webService;
    }

    public void setWebService(IWebService webService) {
        this.webService = webService;
    }

    public IEmailService getEmailService() {
        return emailService;
    }

    public void setEmailService(IEmailService emailService) {
        this.emailService = emailService;
    }

    public LogAnalyzer2(IWebService webService, IEmailService emailService){
        this.webService = webService;
        this.emailService = emailService;
    }

    public void analyze(String fileName) {
        if (fileName.length() < 8) {
            try {
                webService.LogError("this is short name: " + fileName);
            } catch (Exception ex) {
                emailService.sendEmail("lalala@google.com", "can t log", ex.getMessage());
            }
        }
    }
}
