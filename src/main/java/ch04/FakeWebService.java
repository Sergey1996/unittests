package ch04;

public class FakeWebService implements IWebService {
    public String lastError;
    public Exception exception;

    @Override
    public void LogError(String message) throws Exception {
        lastError = message;
        if (exception != null) {
            throw exception;
        }
    }
}
