package ch04;

public interface IEmailService {
    void sendEmail(String to, String subject, String body);
}
