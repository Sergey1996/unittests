package ch10;

import java.math.BigDecimal;

public class EntityX {

    int var1;
    String var2;
    String var3;
    private BigDecimal total;

    public EntityX(int var1, String var2, String var3) {
        this.var1 = var1;
        this.var2 = var2;
        this.var3 = var3;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
