package ch10;

import java.math.BigDecimal;
import java.util.List;

public class ServiceA {


    public void doBusinessOperationXyz(EntityX entityX) {
        ServiceB serviceB = new ServiceB();
        Database.save(entityX);
        List list = Database.find(entityX.var2, entityX);
        BigDecimal total = serviceB.computeTotal(list);
        entityX.setTotal(total);

    }
}
