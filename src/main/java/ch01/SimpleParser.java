package ch01;

public class SimpleParser {
    public int ParseAndSum(String numbers) {
        if (numbers.length() == 0) {
            return 0;
        } else if (!numbers.contains(",")) {
            return Integer.parseInt(numbers);
        } else {
            throw new NumberFormatException("More one number");
        }
    }
}
