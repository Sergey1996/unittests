package ch01;

public class SimpleParserTest {

    public static void TestReturnsZeroWhenEmptyString() {
        try {
            SimpleParser simpleParser = new SimpleParser();
            int result = simpleParser.ParseAndSum("2");
            if (result != 0) {
                System.out.println("@”***SimpleParserTests.TestReturnsZeroWhenEmptyString:\n" +
                        "-------------------\n" +
                        "ParseAndSum return 0 ");
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
