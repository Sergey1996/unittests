package ch07.ex3;

public abstract class BaseStringParser implements IStringParser {
    protected String stringToParse;


    public void setStringToParse(String stringToParse) {
        this.stringToParse = stringToParse;
    }
    public abstract String getTextVersionFromHeader();

    public abstract void hasCorrectHeader();
}
