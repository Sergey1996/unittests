package ch07.ex3;

public interface IStringParser {
    String getTextVersionFromHeader();
    void hasCorrectHeader();
}
