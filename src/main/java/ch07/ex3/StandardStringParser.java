package ch07.ex3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StandardStringParser extends BaseStringParser {

    public StandardStringParser(String text) {
        this.stringToParse = text;
    }

    @Override
    public String getTextVersionFromHeader() {
        Matcher matcher = Pattern.compile("version=(.*?);").matcher(stringToParse);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return "";
    }

    @Override
    public void hasCorrectHeader() {

    }

}
