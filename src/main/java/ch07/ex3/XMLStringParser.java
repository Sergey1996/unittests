package ch07.ex3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMLStringParser extends BaseStringParser {

    public XMLStringParser(String text) {
        this.stringToParse = text;
    }

    @Override
    public String getTextVersionFromHeader() {
        Matcher matcher = Pattern.compile("<Header>(.*?)</Header>").matcher(stringToParse);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return "";
    }

    @Override
    public void hasCorrectHeader() {

    }
}
