package ch07.ex1;

public class TimeLogger {
    public static String createMessage(String info) {
       return SystemTime.getNow() + " " + info;
    }
    //Нехитрый трюк состоит в том, что в классе SystemTime имеются
    //специальные функции, позволяющие изменять текущее время во
    //всей системе. Поэтому всюду, где используется этот класс, будут видны те дата и время, которые мы установили.
}
