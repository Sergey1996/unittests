package ch07.ex1;

import java.time.LocalDateTime;

public class SystemTime {
    private static LocalDateTime dateTime;

    private static LocalDateTime now;

    public static void setDateTime(LocalDateTime dateTime) {
        SystemTime.dateTime = dateTime;
    }

    public static void reset() {
        dateTime = LocalDateTime.MIN;
    }

    public static LocalDateTime getNow() {
        if (dateTime != LocalDateTime.MIN) {
            return dateTime;
        }
        return LocalDateTime.now();
    }
}
