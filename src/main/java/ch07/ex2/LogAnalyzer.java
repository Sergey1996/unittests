package ch07.ex2;

public class LogAnalyzer {

    public void analyzer(String fileName) {
        if (fileName.isEmpty()) {
            throw new IllegalArgumentException("file name is null");
        }
        if (fileName.length() < 8) {
            LoggingFacility.log("Short name file: " + fileName);
        }
        // остальная часть метода
    }
}
