package ch07.ex2;

public class LoggingFacility {
    private static ILogger logger;

    public static ILogger getLogger() {
        return logger;
    }

    public static void setLogger(ILogger iLogger) {
        logger = iLogger;
    }

    public static void log(String text) {
        logger.log(text);
    }
}
