package ch11;

public class MySingleton {
    private static MySingleton instance;

    public static MySingleton getInstance() {
        if(instance == null) {
            return new MySingleton();
        }
        return instance;
    }


}
