package ch11;

public class MySingletonHolder {
    private static RealSingletonLogic instance;

    public static RealSingletonLogic getInstance() {
        if(instance == null) {
            return new RealSingletonLogic();
        }
        return instance;
    }
}
