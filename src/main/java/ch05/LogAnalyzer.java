package ch05;

public class LogAnalyzer {

    private ILogger logger;
    public int minNameLenght = 8;

    public LogAnalyzer(ILogger logger) {
        this.logger = logger;
    }

    public void analyze(String fileName) {
        if (fileName.length() < minNameLenght) {
            logger.logError("this is short name: " + fileName);
        }
    }
}
