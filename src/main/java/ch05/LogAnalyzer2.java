package ch05;

public class LogAnalyzer2 {
    private IWebService webService;
    private ILogger iLogger;
    private int minNameLenght;

    public LogAnalyzer2(IWebService webService, ILogger iLogger){
        this.webService = webService;
        this.iLogger = iLogger;
    }

    public int getMinNameLenght() {
        return minNameLenght;
    }

    public void setMinNameLenght(int minNameLenght) {
        this.minNameLenght = minNameLenght;
    }

    public void analyze(String fileName) {
        if (fileName.length() < minNameLenght) {
            try {
                iLogger.logError("this is short name: " + fileName);
            } catch (Exception ex) {
                webService.write("error registr: "+ ex.getMessage());
            }
        }
    }
}
