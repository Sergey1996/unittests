package ch05;

public interface IFileNameRules {
    boolean isValidLogFileName(String name) throws IllegalArgumentException;
}
