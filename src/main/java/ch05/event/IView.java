package ch05.event;

import javax.swing.*;

public interface IView {
    Action action = null;
    void render(String text);
}
