package ch05.event;

public class Presenter {
    private IView view;

    public Presenter(IView view) {
        this.view = view;
        onAction();
    }

    private void onAction() {
        this.view.render("Hello World");
    }
}
