package ch05;

public interface IEmailService {
    void sendEmail(String to, String subject, String body);
}
