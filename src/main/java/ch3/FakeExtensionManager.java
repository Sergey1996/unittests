package ch3;

public class FakeExtensionManager implements IExtensionManager {
    public boolean willBeValid = false;
    public Exception willThrow;

    @Override
    public boolean isValid(String name) throws Exception {
        if (willThrow != null) {
            throw willThrow;
        }
        return willBeValid;
    }
}
