package ch3.factory;

import ch3.IExtensionManager;

public class LogAnalyzer {

    private IExtensionManager manager;

    public LogAnalyzer() {
        this.manager = ExtensionManagerFactory.create();
    }

    public boolean isValidLogFileName(String fileName) throws Exception {
        return manager.isValid(fileName);
    }
}
