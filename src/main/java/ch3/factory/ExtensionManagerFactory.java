package ch3.factory;

import ch3.FileExtensionManager;
import ch3.IExtensionManager;

public class ExtensionManagerFactory {
    private static IExtensionManager manager;

    public static IExtensionManager create() {
        if (manager != null) {
            return manager;
        }
        return new FileExtensionManager();
    }
    public static void setExtensionManager(IExtensionManager manager1){
        manager = manager1;
    }
}
