package ch3;

public class LogAnalyzer {

    private IExtensionManager manager;

    public LogAnalyzer(IExtensionManager manager) {
        this.manager = manager;
    }

    public boolean isValidLogFileName(String fileName) throws Exception {
        return manager.isValid(fileName);
    }
}
