package ch02;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MemCalculatorTest {

    @Test
    public void Sum_ByDefault_ReturnsZero() {
        MemCalculator memCalculator = MakeCalc();
        int lastSum = memCalculator.Sum();
        assertEquals(0, lastSum);
    }

    @Test
    public void Add_WhenCalled_ChangesSum() {
        MemCalculator memCalculator = MakeCalc();
        memCalculator.Add(1);
        int actual = memCalculator.Sum();
        int expected = 1;
        assertEquals(expected, actual);
    }

    private MemCalculator MakeCalc() {
        return new MemCalculator();
    }
}