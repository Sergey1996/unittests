package ch02;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class LogAnalyzerTest {

    private LogAnalyzer logAnalyzer;
    boolean rez;
    String value;

    @Before
    public void init() {
        logAnalyzer = new LogAnalyzer();
    }

    @Parameterized.Parameters(name = "{index}: value: ''{1}'' expected: ''{0}''")
    public static Collection<Object[]> data() {
        return Arrays.asList(
                new Object[]{false, "myFile.foo"},
                new Object[]{true, "myFile.slf"},
                new Object[]{false, "myFile.txt"});
    }

    public LogAnalyzerTest(boolean rez, String value) {
        this.rez = rez;
        this.value = value;
    }

    @Test
    public void isValidLogFileName_BadAndGoodExtension_ReturnsFalseOrTrue() {
        Assert.assertEquals(rez, logAnalyzer.isValidLogFileName(value));
    }

    @Test
    public void IsValidFileName_EmptyFileName_ThrowsException() {
        try {
            logAnalyzer.isValidLogFileName("");
            fail();
        } catch (IllegalArgumentException ex) {
            assertEquals("file name is null", ex.getMessage());
        }
    }

    //Проверка состояния системы
    @Test
    public void IsValidFileName_WhenCalled_ChangesWasLastFileNameValid() {
        LogAnalyzer logAnalyzer1 = new LogAnalyzer();
        logAnalyzer1.isValidLogFileName("file.foo");
        assertFalse(logAnalyzer1.WasLastFileNameValid);
    }
}