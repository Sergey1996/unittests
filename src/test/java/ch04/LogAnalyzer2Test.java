package ch04;

import org.junit.Test;

import static org.junit.Assert.*;

public class LogAnalyzer2Test {

    @Test
    public void analyze_WebServiceThrows_SendsEmail() {
        FakeWebService fakeWebService = new FakeWebService();
        fakeWebService.exception = new Exception("fake exception");
        FakeEmailService fakeEmailService = new FakeEmailService();

        LogAnalyzer2 logAnalyzer2 = new LogAnalyzer2(fakeWebService, fakeEmailService);
        String tooShortFileName = "abc.exe";
        logAnalyzer2.analyze(tooShortFileName);
        assertEquals("lalala@google.com", fakeEmailService.to);
        assertEquals("can t log", fakeEmailService.subject);
        assertEquals("fake exception", fakeEmailService.body);


    }
}