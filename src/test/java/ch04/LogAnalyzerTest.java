package ch04;

import org.junit.Test;

import static org.junit.Assert.*;

public class LogAnalyzerTest {

    @Test
    public void analyze_TooShortFileName_CallsWebService() throws Exception {
        FakeWebService fakeWebService = new FakeWebService();
        LogAnalyzer logAnalyzer = new LogAnalyzer(fakeWebService);
        String tooShortFileName = "abc.ext";
        logAnalyzer.analyze(tooShortFileName);
        String actual = fakeWebService.lastError;
        String expected = "this is short name: abc.ext";
        assertEquals(expected, actual);
    }
}