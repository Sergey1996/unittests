package ch01;

import org.junit.Test;

import static org.junit.Assert.*;

public class SimpleParserTest {

    @Test
    public void parseAndSum() {
        SimpleParser simpleParser = new SimpleParser();
        String val = "";
        int actual = simpleParser.ParseAndSum(val);
        int expected = 0;
        assertEquals(expected, actual);
        val = "234";
        actual = simpleParser.ParseAndSum(val);
        expected = 234;
        assertEquals(expected, actual);
        val = "1,2,3,4,5";
        String err = "More one number";
        try {
            simpleParser.ParseAndSum(val);
        } catch (NumberFormatException ex) {
            assertEquals(err, ex.getMessage());
        }
    }
}