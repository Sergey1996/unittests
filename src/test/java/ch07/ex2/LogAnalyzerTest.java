package ch07.ex2;

import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;

public class LogAnalyzerTest extends BaseTestsClass {

    @Test
    public void analyze_EmptyFile_ThrowsException() {
        fakeTheLogger();
        LogAnalyzer logAnalyzer = new LogAnalyzer();
        try {
            logAnalyzer.analyzer("myEmptyFile.txt");
            fail();
        } catch (IllegalArgumentException ex) {
            assertEquals("file name is null", ex.getMessage());
        }
    }

}