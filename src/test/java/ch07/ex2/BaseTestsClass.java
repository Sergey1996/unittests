package ch07.ex2;

import org.junit.After;
import org.junit.Test;

import static org.mockito.Mockito.mock;

public class BaseTestsClass {

    public ILogger fakeTheLogger() {
        ILogger iLogger = mock(ILogger.class);
        LoggingFacility.setLogger(iLogger);
        return LoggingFacility.getLogger();
    }

    @After
    public void teardown() {
        LoggingFacility.setLogger(null);
    }
}
