package ch07.ex2;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ConfigurationManagerTest extends BaseTestsClass {

    @Test
    public void analyze_EmptyFile_ThrowsException() {
        ILogger logger = fakeTheLogger();
        ConfigurationManager configurationManager = new ConfigurationManager();
        configurationManager.isConfigured("something");
        verify(logger).log("Проверяется: something");

    }
}