package ch07.ex3.ex5;

import ch07.ex3.StandardStringParser;

public class StandardParserGenericTests extends GenericParserTests<StandardStringParser> {
    @Override
    protected String headerVersion_SingleDigit() {
        return String.format("header\tversion=%s;\t\n", EXPECTED_SINGLE_DIGIT);
    }

    @Override
    protected String headerVersion_WithMinorVersion() {
        return String.format("header\tversion=%s;\t\n", EXPECTED_WITH_MINORVERSION);

    }

    @Override
    protected String headerVersion_WithRevision() {
        return String.format("header\tversion=%s;\t\n", EXPECTED_WITH_REVISION);
    }
}
