package ch07.ex3.ex5;

import ch07.ex3.IStringParser;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

public abstract class GenericParserTests<T extends IStringParser> {

    protected final static String EXPECTED_SINGLE_DIGIT = "1";
    protected final static String EXPECTED_WITH_REVISION = "1.1.1";
    protected final static String EXPECTED_WITH_MINORVERSION ="1.1";

    protected abstract String headerVersion_SingleDigit();
    protected abstract String headerVersion_WithMinorVersion();
    protected abstract String headerVersion_WithRevision();

    private T parser;

    public T getParser(String input) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        return (T) parser.getClass().getConstructor(String.class).newInstance(input);
    }

    @Test
    public void getStringVersionFromHeader_SingleDigit_Found() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        String input  = headerVersion_SingleDigit();
        String version = getParser(input).getTextVersionFromHeader();
        Assert.assertEquals(EXPECTED_SINGLE_DIGIT, version);
    }

    @Test
    public void getStringVersionFromHeader_WithRevision_Found() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        String input  = headerVersion_WithRevision();
        String version = getParser(input).getTextVersionFromHeader();
        Assert.assertEquals(EXPECTED_WITH_REVISION, version);
    }

    @Test
    public void getStringVersionFromHeader_WithMinorVersion_Found() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        String input  = headerVersion_WithMinorVersion();
        String version = getParser(input).getTextVersionFromHeader();
        Assert.assertEquals(EXPECTED_WITH_MINORVERSION, version);
    }
}
