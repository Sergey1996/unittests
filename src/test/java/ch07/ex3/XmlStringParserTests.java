package ch07.ex3;


import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class XmlStringParserTests extends TemplateStringParserTests {

    protected IStringParser getParser(String input) {
        return new XMLStringParser(input);
    }

    @Override
    public void TestGetStringVersionFromHeader_SingleDigit_Found() {
        IStringParser iStringParser = getParser("<Header>1</Header>");
        String actual = iStringParser.getTextVersionFromHeader();
        String expected = "1";
        Assert.assertEquals(expected,actual);
    }

    @Override
    public void TestGetStringVersionFromHeader_WithMinorVersion_Found() {
        IStringParser iStringParser = getParser("<Header>1.2</Header>");
        String actual = iStringParser.getTextVersionFromHeader();
        String expected = "1.2";
        Assert.assertEquals(expected,actual);
    }

    @Override
    public void TestGetStringVersionFromHeader_WithRevision_Found() {
        IStringParser iStringParser = getParser("<Header>1.2.2</Header>");
        String actual = iStringParser.getTextVersionFromHeader();
        String expected = "1.2.2";
        Assert.assertEquals(expected,actual);
    }
}
