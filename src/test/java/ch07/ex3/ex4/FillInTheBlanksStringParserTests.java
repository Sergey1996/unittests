package ch07.ex3.ex4;

import ch07.ex3.IStringParser;
import org.junit.Assert;
import org.junit.Test;

public abstract class FillInTheBlanksStringParserTests {
    protected abstract IStringParser getParser(String input);
    protected abstract String headerVersion_SingleDigit();
    protected abstract String headerVersion_WithMinorVersion();
    protected abstract String headerVersion_WithRevision();
    protected final static String EXPECTED_SINGLE_DIGIT = "1";
    protected final static String EXPECTED_WITH_REVISION = "1.1.1";
    protected final static String EXPECTED_WITH_MINORVERSION ="1.1";

    @Test
    public void getStringVersionFromHeader_SingleDigit_Found() {
        String input  = headerVersion_SingleDigit();
        IStringParser parser = getParser(input);
        String version = parser.getTextVersionFromHeader();
        Assert.assertEquals(EXPECTED_SINGLE_DIGIT, version);
    }

    @Test
    public void getStringVersionFromHeader_WithRevision_Found() {
        String input  = headerVersion_WithRevision();
        IStringParser parser = getParser(input);
        String version = parser.getTextVersionFromHeader();
        Assert.assertEquals(EXPECTED_WITH_REVISION, version);
    }

    @Test
    public void getStringVersionFromHeader_WithMinorVersion_Found() {
        String input  = headerVersion_WithMinorVersion();
        IStringParser parser = getParser(input);
        String version = parser.getTextVersionFromHeader();
        Assert.assertEquals(EXPECTED_WITH_MINORVERSION, version);
    }
}
