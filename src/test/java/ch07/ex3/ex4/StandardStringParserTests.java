package ch07.ex3.ex4;

import ch07.ex3.IStringParser;
import ch07.ex3.StandardStringParser;

public class StandardStringParserTests extends FillInTheBlanksStringParserTests {
    @Override
    protected IStringParser getParser(String input) {
        return new StandardStringParser(input);
    }

    @Override
    protected String headerVersion_SingleDigit() {
        return String.format("header\tversion=%s;\t\n",EXPECTED_SINGLE_DIGIT);
    }

    @Override
    protected String headerVersion_WithMinorVersion() {
        return String.format("header\tversion=%s;\t\n",EXPECTED_WITH_MINORVERSION);
    }

    @Override
    protected String headerVersion_WithRevision() {
        return String.format("header\tversion=%s;\t\n",EXPECTED_WITH_REVISION);
    }
}
