package ch07.ex3;

import org.junit.Test;

import static org.junit.Assert.*;

public class StandardStringParserTest {

    private StandardStringParser getParser(String input) {
        return new StandardStringParser(input);
    }

    @Test
    public void getStringVersionFromHeader_SingleDigit_Found() {
        String input = "header;version=1;\n";
        StandardStringParser parser = getParser(input);
        String version = parser.getTextVersionFromHeader();
        assertEquals("1", version);
    }

    @Test
    public void getStringVersionFromHeader_WithMinorVersion_Found() {
        String input = "header;version=1.2;\n";
        StandardStringParser parser = getParser(input);
        String version = parser.getTextVersionFromHeader();
        assertEquals("1.2", version);
    }

    @Test
    public void getStringVersionFromHeader_WithRevision_Found() {
        String input = "header;version=1.2.1;\n";
        StandardStringParser parser = getParser(input);
        String version = parser.getTextVersionFromHeader();
        assertEquals("1.2.1", version);
    }
}