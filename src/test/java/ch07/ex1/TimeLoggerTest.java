package ch07.ex1;

import ch07.ex1.SystemTime;
import ch07.ex1.TimeLogger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

public class TimeLoggerTest {

    @Test
    public void settingSystemTime_Always_ChangesTime() {
        SystemTime.setDateTime(LocalDateTime.of(2018, 2, 23, 12, 20));
        String actual = TimeLogger.createMessage("a");
        String expected = "2018-02-23T12:20 a";
        Assert.assertEquals(expected, actual);
    }

    @After
    public void cleanTime() {
        SystemTime.reset();
    }
}