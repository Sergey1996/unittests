package ch10;

import junit.framework.TestCase;
import mockit.Mock;
import mockit.MockUp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;

public class ServiceATest {

    private final BigDecimal total = new BigDecimal("125.40");
    private boolean serviceMethodCalled = false;
    private static final Object OBJECT_PARAM = new Object();

    public static class MockDatabase extends Database {
        static int findMethodCallCount;
        static int saveMethodCallCount;

        public static void save(Object o) {
            assertNotNull(o);
            saveMethodCallCount++;
        }

        public static List find(String ql, Object arg1) {
            assertNotNull(ql);
            assertNotNull(arg1);
            findMethodCallCount++;
            return Collections.EMPTY_LIST;
        }

    }

    public class MockServiceB extends ServiceB {
        @Override
        public BigDecimal computeTotal(List items) {
            assertNotNull(items);
            serviceMethodCalled = true;
            return total;
        }
    }


   /* @Before
    public void setUp() throws Exception {
        MockDatabase.saveMethodCallCount = 0;
        MockDatabase.findMethodCallCount = 0;

    }*/

    @Test
    public void testDoBusinessOperationXyz() throws Exception {
        final BigDecimal total = new BigDecimal("125.40");
        EntityX data = new EntityX(5, "abc", "5453 - 1");
        new MockUp<ServiceB>() {
            @Mock
            public BigDecimal computeTotal(List items) {
                //assertNotNull(items);
                //serviceMethodCalled = true;
                return new BigDecimal("125.40");
            }
        };

        new ServiceA().doBusinessOperationXyz(data);

        assertEquals(total, data.getTotal());
        assertTrue(serviceMethodCalled);
        assertEquals(1, MockDatabase.findMethodCallCount);
        assertEquals(1, MockDatabase.saveMethodCallCount);
    }

}