package ch10;

import junit.framework.TestCase;
import org.junit.Ignore;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
@Ignore
public class ServiceATest2 extends TestCase {
    private boolean serviceMethodCalled;

    public static class MockDatabase {
        static int findMethodCallCount;
        static int saveMethodCallCount;

        public static void save(Object o) {
            assertNotNull(o);
            saveMethodCallCount++;
        }

        public static List find(String ql, Object arg1) {
            assertNotNull(ql);
            assertNotNull(arg1);
            findMethodCallCount++;
            return Collections.EMPTY_LIST;
        }
    }

    protected void setUp() throws Exception {
        super.setUp();
        MockDatabase.findMethodCallCount = 0;
        MockDatabase.saveMethodCallCount = 0;
        //Mockit.redefineMethods(Database.class, MockDatabase.class);
    }

    public void testDoBusinessOperationXyz() throws Exception {
        final BigDecimal total = new BigDecimal("125.40");
        /*Mockit.redefineMethods(ServiceB.class,
                new Object() {
                    public BigDecimal computeTotal(List items) {
                        assertNotNull(items);
                        serviceMethodCalled = true;
                        return total;
                    }
                });*/
        EntityX data = new EntityX(5, "abc", "5453-1");
        new ServiceA().doBusinessOperationXyz(data);
        assertEquals(total, data.getTotal());
        assertTrue(serviceMethodCalled);
        assertEquals(1, MockDatabase.findMethodCallCount);
        assertEquals(1, MockDatabase.saveMethodCallCount);
    }
}