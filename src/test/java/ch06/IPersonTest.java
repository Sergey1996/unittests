package ch06;

import org.junit.Assert;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class IPersonTest {

    @Test
    public void  recursiveFakes_work() {
        IPerson person = mock(IPerson.class);
        Assert.assertNotNull(person.getManager());
        Assert.assertNotNull(person.getManager().getManager());
        Assert.assertNotNull(person.getManager().getManager().getManager());
    }
}