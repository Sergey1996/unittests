package ch3.factory;

import ch3.FakeExtensionManager;
import org.junit.Test;

import static org.junit.Assert.*;

public class LogAnalyzerTest {

    @Test
    public void IsValidFileName_SupportedExtension_ReturnsTrue() throws Exception {
        FakeExtensionManager fakeExtensionManager = new FakeExtensionManager();
        fakeExtensionManager.willBeValid = true;
        ExtensionManagerFactory.setExtensionManager(fakeExtensionManager);

        LogAnalyzer logAnalyzer = new LogAnalyzer();
        boolean res = logAnalyzer.isValidLogFileName("file.txt");

        assertTrue(res);
    }
}