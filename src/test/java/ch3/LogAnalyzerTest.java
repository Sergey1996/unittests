package ch3;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class LogAnalyzerTest {

    @Test
    public void IsValidFileName_NameSupportedExtension_ReturnsTrue() throws Exception {
        FakeExtensionManager fakeExtensionManager = new FakeExtensionManager();
        fakeExtensionManager.willBeValid = true;
        LogAnalyzer logAnalyzer = new LogAnalyzer(fakeExtensionManager);
        boolean actual = logAnalyzer.isValidLogFileName("short.exe");
        assertTrue(actual);
    }

    @Test
    public void IsValidFileName_ExtManagerThrowsException_ThrowsException() throws Exception {
        FakeExtensionManager fakeExtensionManager = new FakeExtensionManager();
        fakeExtensionManager.willThrow = new IllegalArgumentException("this is fake");
        LogAnalyzer logAnalyzer = new LogAnalyzer(fakeExtensionManager);
        try {
            logAnalyzer.isValidLogFileName("test");
            fail();
        } catch (IllegalArgumentException ex) {
            assertEquals("this is fake", ex.getMessage());
        }
    }
}