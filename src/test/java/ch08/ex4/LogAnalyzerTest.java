package ch08.ex4;

import ch08.ex1.AnalyzedOutput;
import ch08.ex1.LogAnalyzer;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class LogAnalyzerTest {

    @Test
    public void analyze_SimpleStringLine_UsesDefaulTabDelimiterToParseFields() {
        LogAnalyzer logAnalyzer = new LogAnalyzer();
        AnalyzedOutput actual = logAnalyzer.analyze("10:05\tOpen\tRoy");
        Assert.assertEquals("10:05",actual.getLine(0));
        Assert.assertEquals("Open",actual.getLine(1));
        Assert.assertEquals("Roy",actual.getLine(2));
    }

    @Test
    public void analyze_SimpleStringLine_UsesDefaulTabDelimiterToParseFields2() {
        LogAnalyzer logAnalyzer = new LogAnalyzer();
        AnalyzedOutput expected = new AnalyzedOutput();
        expected.addLine("10:05", "Open", "Roy");

        AnalyzedOutput actual = logAnalyzer.analyze("10:05\tOpen\tRoy");

        Assert.assertEquals(expected, actual);
    }
}