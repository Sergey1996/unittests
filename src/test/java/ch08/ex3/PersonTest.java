package ch08.ex3;

import ch08.ex2.Person;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class PersonTest {

    //антипаттерн - повреждение разделяемого состояния

    Person person = new Person();

    @Test
    public void createAnalyzer_GoodFileName_ReturnsTrue() {
        person.addNumber("055-22334232");
        String actual = person.findPhoneStartingWith("055");
        Assert.assertEquals("055-22334232",actual);
    }

    @Test
    public void findPhoneStartingWith_NoNumbers_ReturnsNull() {
        String actual = person.findPhoneStartingWith("0");
        Assert.assertEquals(null, actual);
    }
}