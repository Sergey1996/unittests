package ch08.ex2;

import ch08.ex1.LogAnalyzer;
import org.junit.Test;

import static org.junit.Assert.*;

public class LogAnalyzerTest {

    //антипаттерн - скрытый вызов теста
    //антипаттерн - ограничение на порядок тестов

    LogAnalyzer logAnalyzer;

    @Test
    public void  createAnalyzer_GoodNameAndBadNameUsage(){
        logAnalyzer = new LogAnalyzer();
        logAnalyzer.init();
        boolean valid = logAnalyzer.isValidLogFileName("abc");
        assertEquals(false,valid);
        createAnalyzer_GoodFileName_ReturnsTrue();

    }

    public void createAnalyzer_GoodFileName_ReturnsTrue() {
        boolean valid = logAnalyzer.isValidLogFileName("abx.exe");
        assertEquals(true,valid);
    }
}