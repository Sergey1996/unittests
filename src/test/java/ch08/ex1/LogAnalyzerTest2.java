package ch08.ex1;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LogAnalyzerTest2 {

    LogAnalyzer logAnalyzer;

    @Before
    public void init() {
        logAnalyzer = new LogAnalyzer();
        logAnalyzer.init();
    }

    @Test
    public void isValidLogFileName() {
        boolean actual = logAnalyzer.isValidLogFileName("abc");
        Assert.assertFalse(actual);
        Assert.assertTrue(logAnalyzer.isValidLogFileName("abc.exe"));
    }
}