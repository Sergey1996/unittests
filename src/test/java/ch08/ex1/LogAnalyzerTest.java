package ch08.ex1;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class LogAnalyzerTest {

    public static LogAnalyzer makeDefaultAnalyzer() {
        LogAnalyzer logAnalyzer = new LogAnalyzer();
        logAnalyzer.init();
        return logAnalyzer;
    }

    @Test
    public void isValidLogFileName() {
        LogAnalyzer logAnalyzer = makeDefaultAnalyzer();
        boolean actual = logAnalyzer.isValidLogFileName("abc");
        Assert.assertFalse(actual);
        Assert.assertTrue(logAnalyzer.isValidLogFileName("abc.exe"));
    }

}