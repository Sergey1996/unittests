package ch05;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class LogAnalyzerTest {

    @Test
    public void analyze_TooShortFileName_CallLogger() {
        ILogger iLogger = mock(ILogger.class);
        LogAnalyzer logAnalyzer = new LogAnalyzer(iLogger);
        logAnalyzer.minNameLenght = 6;
        logAnalyzer.analyze("a.txt");
        Mockito.verify(iLogger, Mockito.atLeastOnce()).logError("this is short name: a.txt");
        verify(iLogger).logError(anyString());
    }

    @Test
    public void returns_ByDefault_WorksForHardCodedArgument() {
        IFileNameRules fileNameRules = mock(IFileNameRules.class);
        when(fileNameRules.isValidLogFileName(anyString())).thenReturn(true);
        assertEquals(true,fileNameRules.isValidLogFileName("test.txt"));
    }

    @Test
    public void Returns_ArgAny_Throws() throws IllegalArgumentException{
        IFileNameRules fileNameRules = mock(IFileNameRules.class);
        doThrow(new IllegalArgumentException("fake ex")).when(fileNameRules).isValidLogFileName(anyString());
        try {
            fileNameRules.isValidLogFileName(anyString());
            fail();
        } catch (IllegalArgumentException ex) {
            assertEquals("fake ex",ex.getMessage());
        }
    }


}