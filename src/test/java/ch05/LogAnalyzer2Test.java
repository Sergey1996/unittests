package ch05;

import org.junit.Test;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class LogAnalyzer2Test {

    @Test
    public void analyze_LoggerThrows_CallsWebService() {
        IWebService webService = mock(IWebService.class);
        ILogger logger = mock(ILogger.class);
        doThrow(new IllegalArgumentException("fake exception")).when(logger).logError(anyString());
        LogAnalyzer2 analyzer2 = new LogAnalyzer2(webService, logger);
        analyzer2.setMinNameLenght(10);
        analyzer2.analyze("short.txt");
        verify(webService).write("error registr: fake exception");

    }
}