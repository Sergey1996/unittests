package ch05.event;

import org.junit.Test;

import javax.swing.*;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class PresenterTest {

    @Test
    public void ctor_WhenViewIsLoaded_CallsViewRender() {
        IView view = mock(IView.class);
        Presenter presenter = new Presenter(view);
        Action action  = mock(Action.class);
        verify(view).render("Hello World");
    }

}